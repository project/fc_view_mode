<?php

/**
 * @file
 * Administration page callbacks and forms.
 */

/**
 * General configuration form.
 */
function fc_view_mode_admin_settings($form, $form_state) {
  $form['fc_view_mode_form_ux_helper'] = array(
    '#type' => 'checkbox',
    '#title' => t('Limit fields based on view mode'),
    '#default_value' => (int) variable_get('fc_view_mode_form_ux_helper', 0),
    '#description' => t('If checked, when editing/creating content with Field Collection Switch View Modes, only fields available in the selected view mode, or fields not set up for display in any view mode, are displayed.'),
  );

  return system_settings_form($form);
}
